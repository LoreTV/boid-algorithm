var boids = [];

var anzBoids = 150;

// =================== GUI Variablen ===================

var gui;
var isGuiShown = true;

var anzahlBoids = 150;

var alignRadius = 25;

var separateRadius = 24;

var cohesionRadius = 50;

var alignForce = 0.75;

var separateForce = 1;

var cohesionForce = 0.5;

var showRadius = ['None', 'Align', 'Separate', 'Cohesion'];

var speed = 5;

var force = 0.5;


// =================== GUI Variablen Einstellungen ===================
var anzahlBoidsMin = 0;
var anzahlBoidsMax = 500;
var anzahlBoidsStep = 1;

var alignRadiusMin = 0;
var alignRadiusMax = 1000;
var alignRadiusStep = 1;

var separateRadiusMin = 0;
var separateRadiusMax = 1000;
var separateRadiusStep = 1;

var cohesionRadiusMin = 0;
var cohesionRadiusMax = 1000;
var cohesionRadiusStep = 1;

var alignForceMin = 0;
var alignForceMax = 1;
var alignForceStep = 0.01;

var separateForceMin = 0;
var separateForceMax = 1;
var separateForceStep = 0.01;

var cohesionForceMin = 0;
var cohesionForceMax = 1;
var cohesionForceStep = 0.01;

var speedMin = 0;
var speedMax = 5;
var speedStep = 0.5;

var forceMin = 0;
var forceMax = 1;
var forceStep = 0.01;

function setup() {
    anzBoids = anzahlBoids;

    // Erstelle Fenster
    createCanvas(windowWidth, windowHeight);
    rectMode(CENTER);

    if (gui == undefined) {
        gui = createGui("Einstellungen (hide with key 'h')");

        //sliderRange(0, 1000, 10);
        gui.addGlobals('anzahlBoids', 'alignRadius', 'separateRadius', 'cohesionRadius', 'alignForce', 'separateForce', 'cohesionForce', 'showRadius', 'speed', 'force');

        gui.show();
    }


    // Erzeuge {anzBoids} Boid Objekte
    for (let i = 0; i < anzBoids; i++) {
        boids.push(new Boid());
    }

}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function draw() {

    if (anzahlBoids != anzBoids) {
        boids = [];
        setup();
    }

    // Hintergrundfarbe
    background(50);

    // Regeln anwenden
    for (const boid of boids) {
        boid.applyForces(boids);
        boid.update();
        boid.edges();
        boid.show();
    }

    if (anzBoids > 0) {
        boids[0].drawRadius();
    }
}

function keyPressed() {
    if (keyCode == 72 && isGuiShown) {
        isGuiShown = false;
        gui.hide();
    } else if (keyCode == 72 && !isGuiShown) {
        isGuiShown = true;
        gui.show();
    }
}