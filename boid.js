class Boid {
    constructor() {
        this.position = createVector(random(windowWidth), random(windowHeight));
        this.geschwindigkeit = p5.Vector.random2D();
        this.geschwindigkeit.setMag(force);
        this.beschleunigung = createVector();
    }

    // Bewege den Boid
    update() {
        this.position.add(this.geschwindigkeit);
        this.geschwindigkeit.add(this.beschleunigung);
        this.geschwindigkeit.limit(speed);
        this.beschleunigung.mult(0);
    }

    /*  Erste Regel: Alignement
        Der Boid versucht die durschnittliche Richtung seiner Nachbarn anzunehmen, die in einem bestimmten Radius sind
        Der Boid ändert seine Richtung allerdings nicht direkt, sondern "steuert" in diese Richtung.
    */
    align(boids) {

        // Radius der Wahrnehmung
        let radius = alignRadius;

        let avgGeschwindigkeit = createVector();

        // Berechnet durschnittliche Bewegungsrichtung der Nachbarn
        let sum = 0;
        for (const boid of boids) {
            let d = dist(this.position.x, this.position.y, boid.position.x, boid.position.y);

            if (boid != this && d < radius) {
                sum++;
                avgGeschwindigkeit.add(boid.geschwindigkeit);
            }
        }

        if (sum > 0) {
            avgGeschwindigkeit.div(sum);

            // Setzt den Bertrag des Richtungsvektors auf die max. Geschwindigkeit.
            // Somit streben die Boids die Regeln immer mit maximaler Geschwindigkeit an.
            avgGeschwindigkeit.setMag(speed);

            // Bewirkt eine "indirekte Steuerung" in die berechnete Richtung. 
            avgGeschwindigkeit.sub(this.geschwindigkeit);

            // Setzt das Limit für die wirkende Kraft
            avgGeschwindigkeit.limit(force);
        }

        return avgGeschwindigkeit;
    }

    /*  Zweite Regel: Separation
        Der Boid geht anderen Boids aus dem Weg, wenn diese in einem bestimmten Radius sind.
        Der Boid ändert seine Richtung allerdings nicht direkt, sondern "steuert" in diese Richtung.
    */
    separation(boids) {

        // Radius der Wahrnehmung
        let radius = separateRadius;

        let avgGeschwindigkeit = createVector();

        // Berechnet durschnittliche Abstoßrichtung
        let sum = 0;
        for (const boid of boids) {
            let d = dist(this.position.x, this.position.y, boid.position.x, boid.position.y);

            if (boid != this && d < radius) {

                // Vektor von dem Boid zu uns
                let aenderung = p5.Vector.sub(this.position, boid.position);

                // Aenderung ist antiproportional zu d, also 1/d
                // Je weiter weg ein Boid ist, desto weniger Abstoßungskraft
                if (d != 0) {
                    aenderung.div(d * d);
                }

                avgGeschwindigkeit.add(aenderung);
                sum++;
            }
        }

        if (sum > 0) {
            avgGeschwindigkeit.div(sum);

            // Setzt den Bertrag des Richtungsvektors auf die max. Geschwindigkeit.
            // Somit streben die Boids die Regeln immer mit maximaler Geschwindigkeit an.
            avgGeschwindigkeit.setMag(speed);

            // Bewirkt eine "indirekte Steuerung" in die berechnete Richtung.
            avgGeschwindigkeit.sub(this.geschwindigkeit);

            // Setzt das Limit für die wirkende Kraft
            avgGeschwindigkeit.limit(force);
        }

        return avgGeschwindigkeit;
    }

    /*  Dritte Regel: Cohesion
        Der Boid versucht den durschnittlichen "Schwerpunkt" seiner Nachbarn anzunehmen, die in einem bestimmten Radius sind
        Der Boid ändert seine Richtung allerdings nicht direkt, sondern "steuert" in diese Richtung.
    */
    cohesion(boids) {

        // Radius der Wahrnehmung
        let radius = cohesionRadius;

        let avgGeschwindigkeit = createVector();

        // Berechnet Massenmittelpunkt
        let sum = 0;
        for (const boid of boids) {
            let d = dist(this.position.x, this.position.y, boid.position.x, boid.position.y);

            if (boid != this && d < radius) {
                avgGeschwindigkeit.add(boid.position);
                sum++;
            }
        }

        if (sum > 0) {
            avgGeschwindigkeit.div(sum);

            // Vektor in Richtung durschnittlicher Massenmittelpunkt
            avgGeschwindigkeit.sub(this.position);

            // Setzt den Bertrag des Richtungsvektors auf die max. Geschwindigkeit.
            // Somit streben die Boids die Regeln immer mit maximaler Geschwindigkeit an.
            avgGeschwindigkeit.setMag(speed);

            // Bewirkt eine "indirekte Steuerung" in die berechnete Richtung.
            avgGeschwindigkeit.sub(this.geschwindigkeit);

            // Setzt das Limit für die wirkende Kraft
            avgGeschwindigkeit.limit(force);
        }

        return avgGeschwindigkeit;

    }

    applyForces(boids) {

        // Variablen für die Stärke der einzelnen Kräfte
        let alignMag = alignForce;
        let separateMag = separateForce;
        let cohesionMag = cohesionForce;

        let richtungsaenderung = this.align(boids);
        richtungsaenderung.mult(alignMag);

        let separateAenderung = this.separation(boids);
        separateAenderung.mult(separateMag);

        let cohesionAenderung = this.cohesion(boids);
        cohesionAenderung.mult(cohesionMag);


        // F = m*a, und wo m = 1; a = F
        // Kraft wird der Beschleunigung hinzugefügt
        this.beschleunigung.add(richtungsaenderung);
        this.beschleunigung.add(separateAenderung);
        this.beschleunigung.add(cohesionAenderung);
    }

    // Prüft , ob der Boid außerhalb des Fensters ist.
    // Wenn ja, dann fliegt er auf der anderen Seite wieder heraus
    edges() {
        if (this.position.x > width) {
            this.position.x = 0;
        } else if (this.position.x < 0) {
            this.position.x = width;
        } else if (this.position.y > height) {
            this.position.y = 0;
        } else if (this.position.y < 0) {
            this.position.y = height;
        }
    }

    drawRadius() {

        // Boidgröße
        strokeWeight(1);

        if (showRadius == "Align") {

            // Boidfarbe
            fill(color('rgba(0, 0, 255, 0.2)'));

            circle(this.position.x, this.position.y, alignRadius);
        } else if (showRadius == "Separate") {
            // Boidfarbe
            fill(color('rgba(209, 46, 46, 0.2)'));

            circle(this.position.x, this.position.y, separateRadius);
        } else if (showRadius == "Cohesion") {
            // Boidfarbe
            fill(color('rgba(37, 207, 60, 0.2)'));

            circle(this.position.x, this.position.y, cohesionRadius);
        }
    }

    // Zeichne den Boid
    show() {

        // Boidfarbe
        stroke(255);

        // Boidgröße
        strokeWeight(5);

        // Boid
        //point(this.position.x, this.position.y);

        // Winkel des Geschwindigkeitsvektors + 90° in RADIANS
        // Keine Ahnung warum nochmal + 90°. Den Objekten fehlen 90°
        let richtungswinkel = this.geschwindigkeit.heading() + radians(90);

        // Notwendig: Neuer Draw state
        push();
        translate(this.position.x, this.position.y);
        rotate(richtungswinkel);
        triangle(0, -4, -1, -1, 1, -1);
        pop();
        // Ende Draw state
    }
}